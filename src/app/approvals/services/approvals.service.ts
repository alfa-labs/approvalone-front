import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/core/services/http/http.service';

import { Approval, ApprovalRecord, ApprovalRecords, TotalizerRecords } from '../models/Approval';

@Injectable({
  providedIn: 'root'
})
export class ApprovalsService {
  private type: string;

  constructor(private router: Router, private http: HttpService) {
  }

  getColumns(): string[] {
    return ['status', 'id', 'requester', 'partner', 'requestDate', 'statusApprover', 'approvalDate', 'total', 'actions'];
  }

  public redirectToDetail(id: number, lineNum: string): void {
    this.router.navigate([`/approvals/${this.type}/${id}/${lineNum}`]);
  }

  public getItems(onlyPendings: string, userId: string | null, id?: string): Observable<ApprovalRecords> {
    let query = `type=${this.type}&onlyPending=${onlyPendings}`;
    query += id ? `&recordId=${id}` : '';

    return this.http.get(`/api/v1/approvals/${userId}?${query}`);
  }

  public getTotalizers(onlyPendings: string, userId: string | null): Observable<TotalizerRecords> {
    return this.http.get(`/api/v1/approvals/totalizers/${userId}?onlyPending=${onlyPendings}`);
  }

  public updateStatus(data: Approval): Observable<any> {
    return this.http.put(`/api/v1/approvals/${data.id}`, data);
  }

  public getItemsDetails(recordId: number, userId: string): Observable<ApprovalRecord> {
    return this.http.get(`/api/v1/approvals/detail?userId=${userId}&type=${this.type}&recordId=${recordId}`);
  }

  public getAttachment(attachmentId: string, idLine: string): Observable<any> {
    return this.http.getBlob(`/api/v1/approvals/attachment/${attachmentId}/${idLine}`);
  }

  public setType(type: string): void {
    this.type = type;
  }

  public getColumsDetails(): string[] {
    return ['indicateProduct', 'description', 'quantity', 'price', 'total', 'contractNumber', 'financialBalance', 'account', 'costCenter'];
  }

  public getColumsApproveDetail(): string[] {
    return ['status', 'approver', 'substitute', 'date', 'hour'];
  }

}