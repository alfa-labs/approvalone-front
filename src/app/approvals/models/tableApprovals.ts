export interface TableApproval {
  label: string;
  property: string;
}