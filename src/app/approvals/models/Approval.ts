export interface ApprovalRecords {
  success: boolean;
  message: string;
  data: ApprovalItem[]
}

export interface ApprovalRecord {
  success: boolean;
  message: string;
  data: ApprovalItem
}

export interface ApprovalRequest {
  approved: boolean;
  remarks: string;
}

export interface ApprovalItem {
  header: Header;
  items: Item[];
  attachments: Attachment[];
  detailApprove: DetailApprove[];
}

export interface Header {
  id: number;
  requester: string;
  partner: string;
  requestDate: string;
  approvalDate: string;
  status: string;
  statusApprover: string;
  remarks: string;
  approvalRemarks: string;
  sequence: number;
  total: number;
  lineNum: string;
}

export interface Item {
  description: string;
  quantity: number;
  price: number;
  total: number;
  account: string;
  costCenter: string;
  lineNum: string;
}

export interface Attachment {
  idApproval: string;
  idLine: string;
  fileNamePublic: string;
}

export interface AttachmentInfo {
  fullpath: string;
  filename: string;
}

export interface TotalizerRecords {
  success: boolean;
  message: string;
  data: Totalizers[]
}

export interface Totalizers {
  type: string;
  quantity: number;
}

export interface DetailApprove {
  status: string;
  name: string;
  date: string;
  hour: string;
  substitute: string;
}

export interface Approval {
  id: number;
  approved: boolean;
  type: string;
  lineNum: string;
  userId: number;
  sequence: number;
  remarks: string | null;
}

export const StatusDetail = [
  { key: 'A', value: 'Gerado pelo Aprovador' },
  { key: 'C', value: 'Cancelado' },
  { key: 'P', value: 'Gerado' },
  { key: 'N', value: 'Reprovado' },
  { key: 'W', value: 'Pendente' },
  { key: 'Y', value: 'Aprovado' },
  { key: 'I', value: 'Não Necessário' }
];