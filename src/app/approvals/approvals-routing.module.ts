import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DetailApprovalComponent } from './detail-approval/detail-approval.component';
import { ListApprovalsComponent } from './list-approvals/list-approvals.component';


const routes: Routes = [
  { path: '', component: ListApprovalsComponent },
  { path: 'approvals/:type/:recordId', component: DetailApprovalComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprovalsRoutingModule { }
