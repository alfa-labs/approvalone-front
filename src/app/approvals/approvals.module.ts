import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { ListApprovalsComponent } from './list-approvals/list-approvals.component';
import { DetailApprovalComponent } from './detail-approval/detail-approval.component';
import { ApprovalsRoutingModule } from './approvals-routing.module';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [
    ListApprovalsComponent,
    DetailApprovalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    FlexLayoutModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'pt-BR'}]
})
export class ApprovalsModule { }
