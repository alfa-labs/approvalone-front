import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailApprovalComponent } from './detail-approval.component';

describe('DetailApprovalComponent', () => {
  let component: DetailApprovalComponent;
  let fixture: ComponentFixture<DetailApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
