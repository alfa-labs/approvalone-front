import { stagger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Approval, Attachment, DetailApprove, Item, StatusDetail } from '../models/Approval';
import { ApprovalsService } from '../services/approvals.service';

@Component({
  selector: 'app-detail-approval',
  templateUrl: './detail-approval.component.html',
  styleUrls: ['./detail-approval.component.scss']
})
export class DetailApprovalComponent implements OnInit {

  loadingApprove: boolean = false;
  loadingReject: boolean = false;
  loading: boolean = false;
  displayProgressSpinner: boolean = false;
  items: Item[];
  attachments: Attachment[];
  detailApprove: DetailApprove[];
  columns: string[] = this.approvalsService.getColumsDetails();
  columnsApprove: string[] = this.approvalsService.getColumsApproveDetail();
  formDetailDocument: FormGroup;
  status: string;
  pageTitle: string;
  type: string;
  line: string;
  statusApprover: string;
  userId: any;

  private recordId: number;
  private inscription: Subscription

  constructor(private approvalsService: ApprovalsService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = sessionStorage.getItem('u-id');
    this.initForm();

    this.inscription = this.route.params.subscribe((params) => {
      this.recordId = params['recordId'];
      this.type = params['type'];
      this.line = params['lineNum'];

      this.approvalsService.setType(this.type);

      if (this.type === 'SC') this.pageTitle = "Detalhe da Solicitação de Compras";
      if (this.type === 'PC') this.pageTitle = "Detalhe do Pedido de Compras";
      if (this.type === 'DOC') this.pageTitle = "Detalhe do Documento de Entrada";
      if (this.type === 'CTT') this.pageTitle = "Detalhe do Contrato";

      this.loadDetails();
    });
  }

  onApproveOrDisapprove(approve: boolean): void {
    const { approvalRemarks, sequence } = this.formDetailDocument.value;
    this.loading = true;

    const data: Approval = {
      sequence: sequence,
      id: this.recordId,
      approved: approve,
      type: this.type,
      remarks: approvalRemarks,
      lineNum: this.line,
      userId: parseInt(this.userId)
    };

    if (approve) {
      this.loadingApprove = true;
      this.loadingReject = false;
    }
    else {
      this.loadingApprove = false;
      this.loadingReject = true;
    }

    this.approvalsService.updateStatus(data).subscribe(
      data => {
        this.loading = false;
        this.loadingApprove = false;
        this.loadingReject = false;

        this.router.navigate([`/approvals`]);
      },
      error => {
        this.loading = false;
        this.loadingApprove = false;
        this.loadingReject = false;

        const { message } = error.error;
        const proccessType = approve ? 'aprovar' : 'rejeitar';

        this.openSnackBar(`Erro ao tentar ${proccessType} o registro.`, 'Atenção');
      }
    )
  }

  onDownloadAttachment(id: string, line: string, filename: string): void {
    this.displayProgressSpinner = true;
    this.approvalsService.getAttachment(id, line).subscribe(
      data => {
        const downloadedFile = new Blob([data], { type: data.type });
        const a = document.createElement('a');
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        a.download = filename;
        a.href = URL.createObjectURL(downloadedFile);
        a.target = '_blank';
        a.click();
        document.body.removeChild(a);

        this.displayProgressSpinner = false;
      },
      error => {
        this.openSnackBar('Erro ao obter os Anexos', 'Atenção');
        this.displayProgressSpinner = false;
      }
    )
  }

  onBackToListApproval(): void {
    this.router.navigate([`/approvals`]);
  }

  private loadDetails() {
    this.displayProgressSpinner = true;

    this.approvalsService.getItemsDetails(this.recordId, this.userId).subscribe(
      response => {
        const { data } = response;
        const { header, items, attachments, detailApprove } = data;
        const status = StatusDetail.filter(e => e.key == header.status)[0].value;
        const statusApprover = StatusDetail.filter(e => e.key == header.statusApprover)[0].value;

        this.status = header.status;
        this.statusApprover = header.statusApprover;

        this.formDetailDocument.patchValue({
          requester: header.requester,
          requestDate: header.requestDate,
          approvalDate: header.approvalDate,
          statusApprover: statusApprover,
          sequence: header.sequence,
          status: status,
          approvalRemarks: header.approvalRemarks,
          remarks: header.remarks,
          total: header.total,
          partner: header.partner
        });

        this.items = items;
        this.attachments = attachments;
        this.detailApprove = detailApprove;

        this.displayProgressSpinner = false;
      },
      error => {
        this.openSnackBar('Erro ao carregar os Detalhes', 'Atenção');
        this.displayProgressSpinner = false;
      }
    );
  }

  private initForm(): void {
    this.formDetailDocument = this.fb.group({
      requester: [''],
      requestDate: [''],
      approvalDate: [''],
      sequence: [''],
      status: [''],
      statusApprover: [''],
      approvalRemarks: [''],
      remarks: [''],
      total: [0],
      partner: [''],
    });
  }

  private openSnackBar(message: string, title: string) {
    this.snackBar.open(
      message,
      title,
      { duration: 1500 }
    );
  }
}

