import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ApprovalItem, Header } from '../models/Approval';
import { ApprovalsService } from '../services/approvals.service';


@Component({
  selector: 'app-list-approvals',
  templateUrl: './list-approvals.component.html',
  styleUrls: ['./list-approvals.component.scss']
})
export class ListApprovalsComponent implements OnInit {

  items: ApprovalItem[];
  displayedColumns: string[] = this.approvalsService.getColumns();
  displayProgressSpinner: boolean = false;
  contrastDOC: boolean = false;
  contrastCTT: boolean = false;
  contrastPC: boolean = false;
  contrastSC: boolean = true;
  quantitySC: number = 0;
  quantityPC: number = 0;
  quantityCTT: number = 0;
  quantityDOC: number = 0;
  type: string;
  userId: string | null;
  onlyPendings: string = 'yes';
  filter: string;

  constructor(private approvalsService: ApprovalsService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.type = 'SC';
    this.userId = sessionStorage.getItem('u-id');

    this.loadCards();
    this.loadItems();
  }

  redictToDetail(item: Header): void {
    const { id, lineNum } = item;
    this.approvalsService.redirectToDetail(id, lineNum);
  }

  onFilterById() {
    this.loadItems(this.filter);
  }

  changeOnlyPendings() {
    this.reseltFieldFilter();
    this.loadCards();
    this.loadItems();
  }

  changeToSC(): void {
    this.resetCards();
    this.contrastSC = true;

    this.type = 'SC';
    this.loadItems();
  }

  changeToPC(): void {
    this.resetCards();
    this.contrastPC = true;

    this.type = 'PC';
    this.loadItems();
  }

  changeToCTT(): void {
    this.resetCards();
    this.contrastCTT = true;

    this.type = 'CTT';
    this.loadItems();
  }

  changeToDOC(): void {
    this.resetCards();
    this.contrastDOC = true;

    this.type = 'DOC';
    this.loadItems();
  }

  resetCards(): void {
    this.reseltFieldFilter();
    this.contrastDOC = false;
    this.contrastCTT = false;
    this.contrastPC = false;
    this.contrastSC = false;
  }

  private loadItems(id?: string): void {
    this.approvalsService.setType(this.type);
    this.displayProgressSpinner = true;

    this.approvalsService.getItems(this.onlyPendings, this.userId, id).subscribe(
      response => {
        const { data } = response;

        this.items = data;
        this.displayProgressSpinner = false;
      },
      error => {
        this.openSnackBar('Erro ao carregar os registros', 'Atenção');
        this.displayProgressSpinner = false;
      }
    );
  }

  private loadCards(): void {
    this.displayProgressSpinner = true;
    this.approvalsService.getTotalizers(this.onlyPendings, this.userId).subscribe(
      response => {
        const { data } = response;

        this.quantitySC = data.filter(e => e.type === 'SC')[0].quantity;
        this.quantityPC = data.filter(e => e.type === 'PC')[0].quantity;
        this.quantityCTT = data.filter(e => e.type === 'CTT')[0].quantity;
        this.quantityDOC = data.filter(e => e.type === 'DOC')[0].quantity;
      },
      error => {
        this.displayProgressSpinner = false;
        this.openSnackBar('Erro ao carregar os Cards', 'Atenção');
      }
    );
  }

  private openSnackBar(message: string, title: string) {
    this.snackBar.open(
      message,
      title,
      { duration: 1500 }
    );
  }

  private reseltFieldFilter(){
    this.filter = '';
  }

}
