import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class MainGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.validSession(state.url);
  }

  canActivateChild(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.validSession(state.url);
  }

  private validSession(url: string): boolean {
    if (
      sessionStorage.getItem('s-id') &&
      sessionStorage.getItem('u-id')
    ) {
      return true;
    }

    this.router.navigate(['/login'], { queryParams: { returnUrl: url } });
    return false;
  }
}
