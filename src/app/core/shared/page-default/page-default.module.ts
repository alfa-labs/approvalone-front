import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageDefaultComponent } from './page-default.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';


@NgModule({
  declarations: [
    PageDefaultComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule
  ],
  exports: [
    PageDefaultComponent
  ]
})
export class PageDefaultModule { }
