import { BreakpointObserver } from '@angular/cdk/layout';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { LoginService } from 'src/app/login/services/login.service';

@Component({
  selector: 'app-page-default',
  templateUrl: './page-default.component.html',
  styleUrls: ['./page-default.component.scss']
})
export class PageDefaultComponent implements OnInit, AfterViewInit {
  
  userName: string | null = '';

  @ViewChild(MatSidenav) sidenav!: MatSidenav;
  @Input() title: string = '';

  constructor(private observer: BreakpointObserver,
              private router: Router,
              private loginService: LoginService) { }

  ngOnInit(): void {
    this.userName = sessionStorage.getItem('u-name');
  }

  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });
  }

  navigateToApproval(): void {
    this.router.navigate(['/approvals']);
  }

  exitApp(): void {
    this.loginService.logout();
  }
}