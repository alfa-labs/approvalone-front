import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingOverlayComponent } from './loading-overlay.component';
import { OverlayService } from './service/overlay.service';
import { CustomMaterialModule } from '../custom-material/custom-material.module';


@NgModule({
  declarations: [
    LoadingOverlayComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule
  ],
  exports: [
    LoadingOverlayComponent
  ],
  providers: [
    OverlayService
  ]
})
export class LoadingOverlayModule { }
