import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  constructor() { }

  @Input() title: string = '';
  @Input() quantity: number = 0;
  @Input() primary: boolean = false;

  ngOnInit(): void {
  }

}
