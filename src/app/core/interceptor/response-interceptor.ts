import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginService } from 'src/app/login/services/login.service';

const ENDPOINT_LOGIN = '/api/v1/authentication/login';

@Injectable({
  providedIn: 'root'
})
export class ResponseInteceptor implements HttpInterceptor {
  constructor(private loginService: LoginService){}

  intercept(
    original_request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(original_request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          const { status, url } = event;

          if(status === 401 && (url?.toLocaleLowerCase().indexOf(ENDPOINT_LOGIN) === 0)) {
            this.loginService.logout();
          }
        }
        return event;
      }));
  }
}
