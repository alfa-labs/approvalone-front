import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomMaterialModule } from './shared/custom-material/custom-material.module';
import { PageDefaultModule } from './shared/page-default/page-default.module';
import { CardsComponent } from './shared/cards/cards.component';
import { HttpService } from './services/http/http.service';
import { InterceptorModule } from './interceptor/interceptor.module';
import { LoadingOverlayModule } from './shared/loading-overlay/loading-overlay.module';

@NgModule({
  declarations: [
    CardsComponent,    
  ],
  imports: [
    PageDefaultModule,
    CommonModule,
    CustomMaterialModule,
    ReactiveFormsModule,
    LoadingOverlayModule
  ],
  exports: [
    CardsComponent,
    PageDefaultModule,
    CustomMaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    InterceptorModule,
    LoadingOverlayModule
  ],
  providers: [
    HttpService
  ]
})
export class CoreModule { }
