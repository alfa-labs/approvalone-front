export interface LoginResponse {
    data: LoginResponseDetail
}

export interface LoginResponseDetail {
    sessionId: string;
    user: UserLoginResponse;
}

export interface UserLoginResponse {
    id: number;
    name: string;
    email: string;
}

export interface Login {
    companyDB: string | undefined;
    username: string;
    password: string;
}