import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { HttpService } from 'src/app/core/services/http/http.service';

import { Login, LoginResponse, LoginResponseDetail } from '../models/Login';

const ENDPOINT = '/api/v1/authentication/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpService, private router: Router) { }

  public executeLogin(login: Login): Observable<LoginResponse> {
    return this.http.post(ENDPOINT, login);
  }

  public registerSession(login: LoginResponseDetail): void {
    const { sessionId, user } = login;
    
    sessionStorage.setItem('s-id', sessionId);
    sessionStorage.setItem('u-id', user.id.toString());
    sessionStorage.setItem('u-name', user.name);
  }

  public logout(): void {
    sessionStorage.removeItem('s-id');
    sessionStorage.removeItem('u-id');
    sessionStorage.removeItem('u-name');

    this.router.navigate(['/login']);
  }
}
