import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { Login } from './models/Login';

import { LoginService } from './services/login.service';

const HANA_DB = environment.HANA_DB;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initFormLogin();
  }

  onLoginSubmit(): void {
    this.loading = true;
    const returnUrl = this.route.snapshot.queryParams['returnUrl'];

    if (this.loginForm.valid) {
      const login: Login = {
        ...this.loginForm.value,
        companyDB: HANA_DB
      };

      this.loginService.executeLogin(login).subscribe(
        data => {
          this.loading = false;
          this.loginService.registerSession(data.data);

          if (returnUrl) this.router.navigate([returnUrl]);
          else this.router.navigate(['/approvals']);
        },
        error => {
          const errorDetail = error.statusText;

          if (errorDetail.toLowerCase() === 'unauthorized') {
            this.openSnackBar('Dados de acesso inválidos', 'Atenção');
          } else {
            this.openSnackBar('Falha na comunicação com o Servidor', 'Atenção');
          }

          this.loading = false;

        }
      );
    } else {
      this.loading = false;
      this.openSnackBar('Usuário e Senha devem ser informados.', 'Atenção');
    }
  }

  private initFormLogin(): void {
    this.loginForm = this.fb.group({
      username: [''],
      password: ['']
    });
  }

  openSnackBar(message: string, title: string) {
    this.snackBar.open(
      message,
      title,
      { duration: 1500 }
    );
  }
}

