import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailApprovalComponent } from './approvals/detail-approval/detail-approval.component';
import { ListApprovalsComponent } from './approvals/list-approvals/list-approvals.component';

import { MainGuard } from './core/guards/main-guard';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'approvals',
    component: ListApprovalsComponent,
    canActivate: [MainGuard],
  },
  {
    path: "approvals/:type/:recordId/:lineNum",
    component: DetailApprovalComponent,
    canActivate: [MainGuard],
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
